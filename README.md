# INTRODUCTION
This module is to serve the styled images using Cloudflare's service on
production environment and to fallback to Drupal's default Image Style
management for non-production / local development environments.

Read more about Cloudflare's image resizing service at:
https://developers.cloudflare.com/images/about/

Today most of the sites use one or the other CDN, one of the widely used CDN
is Cloudflare. This provides a nice service to serve scaled / styled images
from CDN itself. But if we use this, there is no way to show images without
custom work on non-production environments where CDN is not used and especially
on local development environments. This module is intended to fix this issue.

Also, there is cost involved with use of the service so it is best to do it per
image style to keep a check on usage (and costs).

* Project page: https://drupal.org/project/cloudflare_image_style

* To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/cloudflare_image_style

# Note
Since the goal is to serve the images from CDN and only to fallback when
not using via CDN it is bit heavy on PHP processing.

# REQUIREMENTS
It requires only Drupal CORE.

# INSTALLATION
Install as any other contrib module, no specific configuration required for
installation.

# CONFIGURATION
Update the configuration of image style as usual, configure the two fields
there.
