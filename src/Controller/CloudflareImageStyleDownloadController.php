<?php

namespace Drupal\cloudflare_image_style\Controller;

use Drupal\image\Controller\ImageStyleDownloadController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Defines a controller to serve image styles.
 */
class CloudflareImageStyleDownloadController extends ImageStyleDownloadController {

  /**
   * Fallback for styled image on non-CF environments.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   * @param string $cf_effect
   *   Cloudflare Style / Effect.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse|\Symfony\Component\HttpFoundation\Response
   *   Deliver the styled image.
   */
  public function deliverFallback(Request $request, string $cf_effect) {
    $styles = $this->entityTypeManager()->getStorage('image_style')->loadByProperties([
      'cf_effect' => $cf_effect,
      'serve_from_cf' => 1,
    ]);

    if (count($styles) === 0) {
      throw new NotFoundHttpException('No style found for given effect');
    }

    return $this->deliver($request, 'public', reset($styles));
  }

}
